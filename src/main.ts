(() => {
  const xyz = (x: number, y: number, z: number) => x + y + z;
  const numbers: [number, number, number] = [1, 2, 3];
  let total = xyz(...numbers); // スプレッド構文
  console.log(total);

  // 残余引数で定義された関数 sum()
  const sum = (...args: number[]) => args.reduce((prev, crnt) => prev + crnt);
  total = sum(...numbers); // スプレッド構文
  console.log(total);
})();
