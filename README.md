# スプレッド構文

JavaScript では スプレッド構文[^スプレッド構文] を利用して、  
以下の様に複数の引数をまとめて渡せる。
```js
const numbers = [1, 2, 3];
const xyz = (x, y, z) => x + y + z;

const total = xyz(...numbers); // スプレッド構文

console.log(total);
```
TypeScript では同様に利用してもエラー (ts2556) となる。  
![error_message](https://gitlab.com/progmemo/typescript/spread-syntax/uploads/11b8fa961b9e311d85954919b2dc77ad/image.png)  

以下の様に タプル型[^タプル型] で記述することでエラーではなくなる  
例 1)
```ts
// numbers をタプル型で宣言
const numbers: [number, number, number] = [1, 2, 3];
// const numbers: number[] の様な配列としての宣言ではエラー

const total = xyz(...numbers); // スプレッド構文
```

例 2)
```ts
// 引数をタプル型にアサーションする
const total = xyz(...numbers as [number, number, number]);
```

引数を渡される関数やメソッドが 残余引数[^残余引数] で定義されている場合はエラーにならない
```ts
// 残余引数で定義された関数 sum()
const sum = (...args: number[]) => args.reduce((prev, crnt) => prev + crnt);
// const sum = (args: number[]) で定義されているとエラー

const total = sum(...numbers); // スプレッド構文
```

[^スプレッド構文]: [スプレッド構文 - JavaScript | MDN](https://developer.mozilla.org/ja/docs/Web/JavaScript/Reference/Operators/Spread_syntax)  
[^タプル型]: [TypeScript: Handbook - Basic Types](https://www.typescriptlang.org/docs/handbook/basic-types.html#tuple)  
[^残余引数]: [残余引数 - JavaScript | MDN](https://developer.mozilla.org/ja/docs/Web/JavaScript/Reference/Functions/rest_parameters)  
